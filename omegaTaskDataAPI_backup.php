<?php
require_once("./omegaTaskObjects.php");

function ss($input){
    $output=implode("",explode("\\",$input));
	return stripslashes($output);
}

class DataAPI{
	private $db;
	private $mainTask;
	public function __construct(){
		$$this->db = new mysqli('omegatask.db.8736381.hostedresource.com', 'omegatask', 'IoD#yYW1d!ac8idDJFC@WE6%XEZB', 'omegatask');
	}
	public function __destruct(){
		$this->db->close();
	}
	
	function log($msg)
	{
		file_put_contents("log.txt","\n".$msg,FILE_APPEND);
	}
	function logError($conn){
		file_put_contents("log.txt","\nError: " . $conn->errno . " | Error: " . $conn->error,FILE_APPEND);
	}
	
	public function testReadTask(){
		$results = array();
		$stmt = $this->db->prepare("SELECT guid, title, start_date, end_date, completion_date, completed, estimated_time, description FROM task");
		if(!$this->db->error){
			if($stmt->execute()){
				if($stmt->bind_result($guid, $title, $start_date, $end_date, $completion_date, $completed, $estimated_time, $description)){
					$i = 0;
					while($stmt->fetch()){
						$results[$i] = new Task(ss($guid), 
												ss($title), 
												ss($start_date), 
												ss($end_date), 
												ss($completion_date), 
												ss($completed), 
												ss($estimated_time), 
												ss($description));
						$i++;
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
		return $results;
	}
	public function testReadTaskSubtaskLink(){
		$results = array();
		$stmt = $this->db->prepare("SELECT guid, task_guid, subtask_guid, order_index FROM task_subtask_link");
		if(!$this->db->error){
			if($stmt->execute()){
				if($stmt->bind_result($guid, $task_guid, $subtask_guid, $order_index)){
					$i = 0;
					while($stmt->fetch()){
						$results[$i] = new LinkTaskSubtask(	$guid, 
															$task_guid, 
															$subtask_guid, 
															$order_index);
						$i++;
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
		return $results;
	}
	public function testReadTaskUserLink(){
		$results = array();
		$stmt = $this->db->prepare('SELECT task_user_link.guid, task_user_link.task_guid, task_user_link.user_guid, CONCAT_WS(" ",users.first_name,users.last_name) as name FROM task_user_link LEFT JOIN users ON task_user_link.user_guid = users.guid');
		if(!$stmt->db->error){
			if($stmt->execute()){
				if($stmt->bind_result($link_guid, $task_guid, $user_guid, $user_display_name)){
					$i = 0; 
					while($stmt->fetch()){
						$results[$i] = new LinkTaskUser($link_guid, $task_guid, $user_guid, $user_display_name);
						$i++;
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno. " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
		return $results;
	}
	public function testReadTaskTeamLink(){
		$results = array();
		$stmt = $this->db->prepare('SELECT task_team_link.guid, task_team_link.task_guid, task_team_link.team_guid, team.title FROM task_team_link LEFT JOIN team on task_team_link.team_guid=team.guid');
		if(!$stmt->db->error){
			if($stmt->execute()){
				if($stmt->bind_result($link_guid, $task_guid, $team_guid, $team_name)){
					$i = 0;
					while($stmt->fetch()){
						$results[$i] = new LinkTaskTeam($link_guid, $task_guid, $team_guid, $team_name);
						$i++;
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
		return $results;
	}
	function testReadUsers(){
		$results = array();
		$stmt = $this->db->prepare('SELECT guid, username, first_name, last_name FROM users');
		if(!$stmt->db->error){
			if($stmt->execute()){
				if($stmt->bind_result($guid, $username, $firstName, $lastName)){
					$i = 0;
					while($stmt->fetch()){
						$results[$i] = new User($guid, $username, $firstName, $lastName);
						$i++;
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
		return $results;
	}
	function testReadTeams(){
		$results = array();
		$stmt = $this->db->prepare('SELECT guid, title FROM team');
		if(!$stmt->db->error){
			if($stmt->execute()){
				if($stmt->bind_result($guid, $title)){
					$i = 0;
					while($stmt->fetch()){
						$results[$i] = new Team($guid, $title);
						$i++;
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
		return $results;
	}
	function testReadTaskNames(){
		$results = array();
		$stmt = $this->db->prepare('SELECT guid, title FROM task');
		if(!$stmt->db->error){
			if($stmt->execute()){
				if($stmt->bind_result($guid, $title)){
					$i = 0;
					while($stmt->fetch()){
						$results[$i] = new Task($guid, $title, NULL, NULL,NULL,NULL,NULL,NULL);
						$i++;
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
		return $results;
	}
	/*
	guid	char(36)	latin1_bin		No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	 Fulltext
	title	text	latin1_swedish_ci		No			 Browse distinct values	 Change	 Drop	Primary	Unique	Index	 Fulltext
	start_date	date			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	end_date	date			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	completion_date	date			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	completed	tinyint(1)			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	estimated_time	int(12)			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	description
	*/
	function fetchTaskGuidFromParams($newTask){
		$stmt = $this->db->prepare('SELECT guid FROM task WHERE title=? AND start_date=? AND end_date=? AND estimated_time=? LIMIT 1');
		if(!$this->db->error){
			if($stmt->bind_param('sssi',$newTask->getTitle(), $newTask->getStartDate(), $newTask->getEndDate(), $newTask->getEstimatedHours())){
				if($stmt->execute()){
					if($stmt->bind_result($guid)){
						while($stmt->fetch()){
							$stmt->close();
							return $guid;
						}
					}
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
	}
	function SubtaskLinks($subtaskGuid, $supertaskGuid, $order){
		$stmt = $this->db->prepare('INSERT INTO task_subtask_link (guid, task_guid, subtask_guid, order_index) VALUES (UUID(), ?, ?, ?)');
			if ($stmt!=NULL)
		{
			echo "STMT NOT NULL";
		}
		else{
			echo "STMT NULL";
		}
		if(!$this->db->error){
			if($stmt->bind_param('ssi',$supertaskGuid, $subtaskGuid, $order)){
				if($stmt->execute()){
					$stmt->close();
					$newTask->setGuid($guid); 
					return $newTask;
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}	}
	function createTask(Task $newTask){
		$result;
		if ($newTask!=NULL)
		{
			echo "NEW TASK DOES NOT EQUAL NULL";
		}
		else{
			echo "NEW TASK EQUALS NULL";
		}
		$stmt = $this->db->prepare('INSERT INTO task (guid, title, start_date, end_date, estimated_time, description) VALUES(UUID(),?,?,?,?,?)');
		if(!$this->db->error){
			if($stmt->bind_param('sssis',$newTask->getTitle(), $newTask->getStartDate(), $newTask->getEndDate(), $newTask->getEstimatedHours(), $newTask->getDescription())){
				if($stmt->execute()){
					$stmt->close();
					$guid = $this->fetchTaskGuidFromParams($newTask);
					$newTask->setGuid($guid); 
					return $newTask;
				}
			}
			if($stmt->error){
				$this->logError($stmt);
				echo "\nError: " . $stmt->errno . " | Error: " . $stmt->error;
			}
			$stmt->close();
		}else{
			$this->logError($this->db);
			echo "\nError: " . $this->db->errno . " | Error: " . $this->db->error;
		}
	}
	function createNestedTaskList($taskList, $subtaskLinks){
		$this->mainTask = NULL;
		$this->mainTask = $this->associateTaskItems($taskList, $subtaskLinks);
		return $this->mainTask;
	}
	function associateTaskItems($taskList, $subtaskLinks){
		foreach($subtaskLinks as $key => $subtaskLink){
			$sourceGuid = $subtaskLink->getSourceGuid();
			$targetGuid = $subtaskLink->getTargetGuid();
			$orderIndex = $subtaskLink->getOrderIndex();
			$sourceTask;
			$targetTask;
			foreach($taskList as $k => $task){
				if($task->getGuid() == $sourceGuid){
					$sourceTask = $task;
				}else if($task->getGuid() == $targetGuid){
					$targetTask = $task;
				}
			}
			if( isset($sourceTask) && isset($targetTask) ){
				$sourceTask->setSubtaskItem( $targetTask, $orderIndex);
			}
		}
		$main;
		foreach($taskList as $k => $task){
			if($task->getIsProjectTask()){
				$main = $task;
			}
		}
		return $main;
	}
	function setupAssignedToNames($taskList, $taskUserLinks, $taskTeamLinks){
		foreach($taskList as $key => $task){
			// try to link to a user first
			foreach($taskUserLinks as $uKey => $userLink){
				if($task->getGuid() == $userLink->getSourceGuid()){
					$task->setAssignedToName($userLink->getUserDisplayName());
				}
			}
			if($task->getAssignedToName() == ''){ // if task couldn't be linked to user then try linking to a team
				foreach($taskTeamLinks as $lKey => $teamLink){
					if($task->getGuid() == $teamLink->getSourceGuid()){
						$task->setAssignedToName($teamLink->getTeamDisplayName());
					}
				}
			}
		}
	}
	function dumpExpandMain($task, $lvl){
		foreach($task as $k => $item){
			echo '<br>';
			echo $item->getTitle() . ' subTask: ';	
			$subTask = $item->getSubtask();
			if(isset($subTask)){
			}else{
				echo 'No Subtask';
			}
			echo '<br>';
		}
	}
	
	
}

$api = new DataAPI();
$f = $_REQUEST["f"];
$xmlDoc;
$jsonDoc;
switch($f){
	case "fetchTaskList":
		$results = array();
		$taskList = $api->testReadTask();
		$subtaskLinks = $api->testReadTaskSubtaskLink();
		$taskUserLinks = $api->testReadTaskUserLink();
		$taskTeamLinks = $api->testReadTaskTeamLink();
		$nestedTaskList = $api->createNestedTaskList($taskList, $subtaskLinks);
		$api->setupAssignedToNames($taskList,$taskUserLinks,$taskTeamLinks);
		$results = array(
			'TaskList' => $taskList,
			'SubtaskLinks' => $subtaskLinks,
			'NestedTaskList' => $nestedTaskList,
			'TaskUserLinks' => $taskUserLinks,
			'TaskTeamLinks' => $taskTeamLinks
		);
		$jsonDoc = new JSONDoc($results);
		$jsonDoc->printJSON();
	break;
	case "fetchUsers":
		$results = array();
		$users = $api->testReadUsers();
		$results["Users"] = $users;
		$jsonDoc = new JSONDoc($results);
		$jsonDoc->printJSON();
	break;
	case "fetchTeams":
		$results = array();
		$teams = $api->testReadTeams();
		$results["Teams"] = $teams;
		$jsonDoc = new JSONDoc($results);
		$jsonDoc->printJSON();
	break;
	case "fetchTaskNames":
		$results = array();
		$tasks = $api->testReadTaskNames();
		$results["Tasks"] = $tasks;
		$jsonDoc = new JSONDoc($results);
		$jsonDoc->printJSON();
	break;
	case "createTask":
		$results = array();
		$_title = $_REQUEST["Title"];
		$_startDate = $_REQUEST["StartDate"];
		$_endDate = $_REQUEST["EndDate"];
		$_estHours = $_REQUEST["EstHours"];
		$_descrip = $_REQUEST["Descrip"];
		$_teamGuid = $_REQUEST["TeamGuid"];
		$_userGuid = $_REQUEST["UserGuid"];
		$_superGuid = $_REQUEST["SuperGuid"];
		$newTask = new Task(0, $_title, $_startDate, $_endDate, 0, 0, $_estHours, $_descrip);
		$task = $api->createTask($newTask);
		
		$results["Task"] = $task;
		$jsonDoc = new JSONDoc($results);
		$jsonDoc->printJSON();
	break;
}
if(isset($xmlDoc)){
	$xmlDoc->printXML();
}

?>