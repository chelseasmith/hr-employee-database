// JavaScript Document

function init(){
	createGraph();
}

function formatter(val, axis) {
    return val.toFixed(axis.tickDecimals);
}

function createGraph(){
		var data = [];
		var idealRemaining = [];
		var remainingTime = [];
		var remainingTask = [];
		var completedTask = [];
		
		var randomDecrement = 0;
		var taskTotal = 25;
		var hoursTotal = 250;
		var currentTotal = taskTotal;
		var currentHours = hoursTotal;
		var days = 21;
		
		for(var i = 0; i < days; i++){
			
			randomDecrement = Math.random() * 2;
			
			if(i != 0){
				currentTotal -= randomDecrement;
				currentTotal = (currentTotal < 0 ? 0 : currentTotal); 
				currentHours -= Math.random() * 20;
				currentHours = (currentHours < 0 ? 0 : currentHours);
			}
			
			if( i >= days - 4 ){
				var dist = days - i;
				currentTotal -= Math.floor( currentTotal/dist);
				currentHours -= Math.floor( currentHours/dist );
				randomDecrement = Math.floor( currentTotal/dist);
			}
			
			if(i != 0){
				idealRemaining.push( [i, hoursTotal - ((hoursTotal/(days-1))*i) ] );
			}else{
				idealRemaining.push( [i, hoursTotal ] );
			}
			remainingTime.push( [i, currentHours ] );
			remainingTask.push( [i, currentTotal ] );
			completedTask.push( [i, randomDecrement ] );
		}
		var data = [
		{
			data: idealRemaining,
			lines: { show: true },
			yaxis: 1,
			label: "Ideal"
		}, {
			data: remainingTime,
			lines: { show: true },
			points: { show: true},
			yaxis: 1,
			label: "Remaining Hours"
		}, {
			data: remainingTask,
			lines: { show: true },
			yaxis: 2,
			label: "Remaining Tasks"
		}, {
			data: completedTask,
			bars: { show: true },
			yaxis: 2,
			label: "Completed Tasks"
		}
		
		];
	
	
		var axes = {
						xaxes: [ { position: "bottom", min: 0, tickDecimals:0, max: days-1 } ],
						yaxes: [ { min: 0 }, { position: "right", min: 0 } ]
					}	
					
					
		
		$.plot("#placeholder", data, axes);
}