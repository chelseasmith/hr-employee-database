<?php
/* task objects */
/*
	guid	char(36)	latin1_bin		No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	 Fulltext
	title	text	latin1_swedish_ci		No			 Browse distinct values	 Change	 Drop	Primary	Unique	Index	 Fulltext
	start_date	date			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	end_date	date			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	completion_date	date			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	completed	tinyint(1)			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	estimated_time	int(12)			No			 Browse distinct values	 Change	 Drop	 Primary	 Unique	 Index	Fulltext
	description
*/

class Task{
	private $guid;
	private $title;
	private $startDate;
	private $endDate;
	private $completionDate;
	private $completed;
	private $estimatedHours;
	private $description;
	private $subTask;
	private $isProjectTask;
	private $assignedToName;
	
	public static function copyTask(Task $input){
		$ouput = new Task($input->guid, $input->title, $input->startDate, $input->endDate, $input->completionDate, $input->completed, $input->estimatedHours, $input->description);
		return $output;
	}
	public function __construct($_guid, $_title, $_sd, $_ed, $_cd, $_completed, $_est, $_desc){
		$this->guid 			= $_guid;
		$this->title 			= $_title;
		$this->startDate 		= $_sd;
		$this->endDate 			= $_ed;
		$this->completionDate 	= $_cd;
		$this->completed 		= $_completed;
		$this->estimatedHours 	= $_est;
		$this->description 		= $_desc;
		$this->subTask 			= array();
		$this->isProjectTask 	= true;
		$this->assignedToName 	= "";
	}
	public function __destruct(){
		$this->guid 			= NULL;
		$this->title 			= NULL;
		$this->startDate 		= NULL;
		$this->endDate 			= NULL;
		$this->completionDate 	= NULL;
		$this->completed 		= NULL;
		$this->estimatedHours 	= NULL;
		$this->description 		= NULL;
		$this->subTask 			= NULL;
		$this->isProjectTask	= NULL;
		$this->assignedToName 	= NULL;
	}
	public function setAssignedToName($_name){
		$this->assignedToName = $_name;	
	}
	public function getAssignedToName(){
		return $this->assignedToName;
	}
	public function getDescription(){
		return $this->description;
	}
	public function setDescription($_description){
		$this->description = $_description;
	}
	public function getEstimatedHours(){
		return $this->estimatedHours;
	}
	public function setEstimatedHours($_estimatedHours){
		$this->estimatedHours = $_estimatedHours;
	}
	public function isCompleted(){
		return $this->completed();
	}
	public function setCompleted($_isCompleted){
		$this->completed = $_isCompleted;
	}
	public function getCompletionDate(){
		return $this->completionDate;
	}
	public function setCompletionDate($_completionDate){
		$this->completionDate = $_completionDate;
	}
	public function getEndDate(){
		return $this->endDate;
	}
	public function setEndDate($_endDate){
		$this->startDate = $_endDate;
	}
	public function getStartDate(){
		return $this->startDate;	
	}
	public function setStartDate($_startDate){
		$this->startDate = $_startDate;
	}
	public function getTitle(){
		return $this->title;
	}
	public function setTitle($_title){
		$this->title = $_title;
	}
	public function getGuid(){
		return $this->guid;
	}
	public function setGuid($_guid){
		$this->guid = $_guid;
	}
	public function setSubtask($_subtask){
		$this->subTask = $_subtask;
	}
	public function getSubtask(){
		return $this->subTask;
	}
	public function setSubtaskItem(Task $_subtask, $_order_index){
		$_subtask->isProjectTask = false;
		$this->subTask[$_order_index] = $_subtask->toJSON();
	}
	public function	getIsProjectTask(){
		return $this->isProject;
	}
	public function toJSON(){
		return json_encode(get_object_vars($this));
	}
}

class User{
	private $guid;
	private $username;
	private $firstName;
	private $lastName;
	function __construct($_guid, $_username, $_firstName, $_lastName){
		$this->guid = $_guid;
		$this->username = $_username;
		$this->firstName = $_firstName;
		$this->lastName = $_lastName;
	}
	function __destruct(){
		$this->guid = NULL;
		$this->username = NULL;
		$this->firstName = NULL;
		$this->lastName = NULL;
	}
	public function getGuid(){
		return $this->guid;
	}
	public function getUsername(){
		return $this->username;
	}
	public function getFirstName(){
		return $this->firstName;
	}
	public function getLastName(){
		return $this->lastName;
	}
	public function toJSON(){
		return json_encode(get_object_vars($this));
	}
}
class Team{
	private $guid;
	private $title;
	function __construct($_guid, $_title){
		$this->guid = $_guid;
		$this->title = $_title;
	}
	function __destruct(){
		$this->guid = NULL;
		$this->title = NULL;
	}
	public function getGuid(){
		return $this->guid;
	}
	public function getTitle(){
		return $this->title;
	}
	public function setGuid($_guid){
		$this->guid = $_guid;
	}
	public function setTitle($_title){
		$this->title = $_title;
	}
	public function toJSON(){
		return json_encode(get_object_vars($this));
	}
}

abstract class Link{
	protected $guid;
	protected $sourceGuid;
	protected $targetGuid;
	
	protected function __construct($_guid, $_sourceGuid, $_targetGuid){
		$this->guid 		= $_guid;
		$this->sourceGuid 	= $_sourceGuid;
		$this->targetGuid 	= $_targetGuid;
	}
	protected function __desctruct(){
		$this->guid 		= NULL;
		$this->sourceGuid 	= NULL;
		$this->targetGuid 	= NULL;
	}
	public function getGuid(){
		return $this->guid;
	}
	public function setGuid($_guid){
		$this->guid = $_guid;
	}
	public function getSourceGuid(){
		return $this->sourceGuid;
	}
	public function setSourceGuid($_sourceGuid){
		$this->sourceGuid = $_sourceGuid;
	}
	public function getTargetGuid(){
		return $this->targetGuid;
	}
	public function setTargetGuid($_targetGuid){
		$this->targetGuid = $_targetGuid;
	}
}

class LinkTaskUser extends Link{
	private $userDisplayName;
	public function __construct($_link_guid, $_task_guid, $_user_guid, $_user_display_name){
		$this->guid 			= $_link_guid;
		$this->sourceGuid 		= $_task_guid;
		$this->targetGuid 		= $_user_guid;
		$this->userDisplayName 	= $_user_display_name;
	}
	public function __destruct(){
		$this->guid 			= NULL;
		$this->sourceGuid 		= NULL;
		$this->targetGuid 		= NULL;
		$this->userDisplayName 	= NULL;
	}
	public function getUserDisplayName(){
		return $this->userDisplayName;
	}
	public function setUserDisplayName($_user_display_name){
		$this->userDisplayName = $_user_display_name;
	}
	public function toJSON(){
		return json_encode(get_object_vars($this));
	}
}
class LinkTaskTeam extends Link{
	private $teamDisplayName;
	public function __construct($_link_guid, $_task_guid, $_team_guid, $_team_display_name){
		$this->guid 			= $_link_guid;
		$this->sourceGuid 		= $_task_guid;
		$this->targetGuid 		= $_team_guid;
		$this->teamDisplayName 	= $_team_display_name;
	}
	public function __destruct(){
		$this->guid 			= NULL;
		$this->sourceGuid 		= NULL;
		$this->targetGuid 		= NULL;
		$this->teamDisplayName 	= NULL;
	}
	public function getTeamDisplayName(){
		return $this->teamDisplayName;
	}
	public function setTeamDisplayName($_team_display_name){
		$this->teamDisplayName = $_team_display_name;
	}
	public function toJSON(){
		return json_encode(get_object_vars($this));
	}
}
class LinkTaskSubtask extends Link{
	private $orderIndex;
	
	public function __construct($_guid, $_sourceGuid, $_targetGuid, $_orderIndex){
		$this->guid 		= $_guid;
		$this->sourceGuid 	= $_sourceGuid;
		$this->targetGuid 	= $_targetGuid;
		$this->orderIndex 	= $_orderIndex;
	}
	public function __destruct(){
		$this->guid 		= NULL;
		$this->sourceGuid 	= NULL;
		$this->targetGuid 	= NULL;
		$this->orderIndex 	= NULL;
	}
	public function getOrderIndex(){
		return $this->orderIndex;
	}
	public function setOrderIndex($_orderIndex){
		$this->orderIndex = $_orderIndex;
	}
	public function toJSON(){
		return json_encode(get_object_vars($this));
	}
}

class JSONDoc{
	public $data;
	function __construct($_data){
		$this->data = $_data;
	}
	function __destruct(){
		$this->data = NULL;
	}
	function recursiveExpand($input){
		$output = '';
		if(is_array($input)){
			if(JSONDoc::is_assoc($input)){
				$output .= '{';
				foreach($input as $key => $val){
					if( $output != '{' ){ $output .= ','; }
					$output .= '"' . $key . '":';
					$output .= $this->recursiveExpand($val);
				}
				$output .= '}';
			}else{
				$output .= '[';
				foreach($input as $key => $val){
					if($output != '['){ $output .= ','; }
					$output .= ($this->recursiveExpand($val));
				}
				$output .= ']';
			}
		}else{
			if(method_exists($input, 'toJSON')){
				$output .= $input->toJSON();
			}else{
				$output .= json_encode(get_object_vars($input));
			}
		}
		return $output;
	}
	function printJSON(){
		// this assumes that $this->data is an associative array
		$json = $this->recursiveExpand($this->data);
		print_r($json);
	}
	public static function toJSONArray($title, $objs){
		$json = '"'.$title.'":[';
		for($i = 0; $i < count($objs); $i++){
			if($i > 0){
				$json .= ',';
			}
			if(method_exists($objs[$i], 'toJSON')){
				$json .= $objs[$i]->toJSON();
			}else{
				$json .= json_encode(get_object_vars($objs[$i]));
			}
		}
		$json .= ']';
		return $json;
	}
	public static function is_assoc($array) {
		return (bool)count(array_filter(array_keys($array), 'is_string'));
	}
}
class XMLDoc{
	public $parentName;
	public $data;
	function __construct($_parentName, $_data){
		$this->parentName = $_parentName;
		$this->data = 		$_data;
	}
	function __destruct(){
		$this->parentName 	= NULL;
		$this->data 		= NULL;
	}
	function recursiveExpand($input){
		$output = '';
		if(is_array($input)){
			foreach($input as $key => $val){
				$output .= ($this->recursiveExpand($val));
			}
		}else{
			$output .= $input->toXML();
		}
		return $output;
	}
	function printXML(){
		header ("Content-Type:text/xml");
		$xml = '<?xml version="1.0"?>';
		$xml.= '<' . $this->parentName . '>';
		$xml.= $this->recursiveExpand($this->data);
		$xml.= '</' . $this->parentName . '>';
		print $xml;
	}
}
?>