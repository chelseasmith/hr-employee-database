function init ()
{
	$.notify.defaults({elementPosition: 'right middle'});
	/*
	alertify.alert('INIT GETTING CALLED', function(){
    alertify.message('OK');
	});
	*/
	fetchTeams();
	fetchUserList();
	fetchParentTasks();
}
function fetchUserList(){
	var params = "f=fetchUsers";
	//var fName = document.forms["create_user"]["first_name"].value;
	var request;
	if (window.XMLHttpRequest){ //IE7+, Firefox, Chrome, Opera, Safari
		request = new XMLHttpRequest();
	}else{//IE6, IE5
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// update the rank value when a response is returned
	request.onreadystatechange=function(){
		if(request.readyState == 4 && request.status == 200){
			// check for error text instead of number
	
			var obj = jQuery.parseJSON( request.responseText );
			var user = obj["Users"];
			populateSelect(user);
			/*
			for (var i = 0; i < user.length; i ++)
			{
				var firstName = user[i].firstName;
				var lastName = user[i].lastName;
			}
			*/
		   	
		
		}else if(request.readyState == 4 && request.status != 200){
			// something failed
		}
	}
	request.open("POST", "omegaTaskDataAPI.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send(params);	
}
function fetchTeams(){
	var params = "f=fetchTeams";
	//var fName = document.forms["create_user"]["first_name"].value;
	var request;
	if (window.XMLHttpRequest){ //IE7+, Firefox, Chrome, Opera, Safari
		request = new XMLHttpRequest();
	}else{//IE6, IE5
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// update the rank value when a response is returned
	request.onreadystatechange=function(){
		if(request.readyState == 4 && request.status == 200){
			// check for error text instead of number
	
			var obj = jQuery.parseJSON( request.responseText );
			var user = obj["Teams"];
			populateTeams(user);
			/*
			for (var i = 0; i < user.length; i ++)
			{
				var firstName = user[i].firstName;
				var lastName = user[i].lastName;
			}
			*/
		   	
		
		}else if(request.readyState == 4 && request.status != 200){
			// something failed
		}
	}
	request.open("POST", "omegaTaskDataAPI.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send(params);	
}
function fetchParentTasks ()
{
		var params = "f=fetchTaskNames";
	//var fName = document.forms["create_user"]["first_name"].value;
	var request;
	if (window.XMLHttpRequest){ //IE7+, Firefox, Chrome, Opera, Safari
		request = new XMLHttpRequest();
	}else{//IE6, IE5
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// update the rank value when a response is returned
	request.onreadystatechange=function(){
		if(request.readyState == 4 && request.status == 200){
			// check for error text instead of number
	
			var obj = jQuery.parseJSON( request.responseText );
			var tasks = obj["Tasks"];
			populateTasks(tasks);
			/*
			for (var i = 0; i < user.length; i ++)
			{
				var firstName = user[i].firstName;
				var lastName = user[i].lastName;
			}
			*/
		   	
		
		}else if(request.readyState == 4 && request.status != 200){
			// something failed
		}
	}
	request.open("POST", "omegaTaskDataAPI.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send(params);	
}
function createTask ()
{
	// {"Task":{"guid":"f3b1def4-7f58-11e4-8a10-001ec95c5e8b",
	//"title":"Nslkjdl","startDate":"2014-12-02","endDate":"2014-12-03",
	//"completionDate":0,"completed":0,"estimatedHours":"16","description":"Shitty McBurger",
	//"subTask":[],"isProjectTask":true,"assignedToName":""}}
	//http://willstowers.com/apps/Final/omegaTaskDataAPI.php?f=createTask
	//&Title=Nslkjdl
	//&StartDate=2014-12-02&EndDate=2014-12-03&EstHours=16&Descrip=Shitty%20McBurger&TeamGuid=0&UserGuid=0
	    var title = document.forms["create_task"]["task_title"].value;
	var monS = document.forms["create_task"]["task_mon"].value;
	var dayS = document.forms["create_task"]["task_day"].value;
	var yearS = document.forms["create_task"]["task_year"].value;
	var monE = document.forms["create_task"]["task_Emonth"].value;
	var dayE = document.forms["create_task"]["task_Eday"].value;
	var yearE = document.forms["create_task"]["task_Eyear"].value;
	var dateStart = yearS + "-" + monS + "-" + dayS;
	var dateEnd = yearE + "-" + monE + "-" + dayE;
	var descr = document.forms["create_task"]["description"].value;
	var hours = document.forms["create_task"]["task_hours"].value;
	//var yearE = document.forms["create_task"]["task"].value;
	var teamSelect = document.forms["create_task"]["team_select"].value;
	//user_select
	var userSelect = document.forms["create_task"]["user_select"].value;
	var taskSelect = document.forms["create_task"]["task_select"].value;
	/*
	http://www.willstowers.com/apps/Final/omegaTaskDataAPI.php?
	f=createTask
	&Title=Nslkjdl
	&StartDate=2014-12-02
	&EndDate=2014-12-03
	&EstHours=16
	&Descrip=Shitty%20McBurger
	&TeamGuid=0
	&UserGuid=0
	*/
		var params = "f=createTask"+
		"&Title="+title+
		"&StartDate=" 
		+dateStart+
		"&EndDate="
		+dateEnd + 
		"&EstHours="
		+hours+
		'&Descrip="'
		+descr+
		'"&UserGuid='
		+userSelect;
	var request;
	if (window.XMLHttpRequest){ //IE7+, Firefox, Chrome, Opera, Safari
		request = new XMLHttpRequest();
	}else{//IE6, IE5
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// update the rank value when a response is returned
	request.onreadystatechange=function(){
		if(request.readyState == 4 && request.status == 200){
			// check for error text instead of number
			
			var obj = jQuery.parseJSON( request.responseText );
			var task = obj["Task"];
			var guid = task.guid;
			if (guid !=null&&guid!=""&&guid!=0)
			{
					alertify.alert('Task Successfully Created', function(){
    				alertify.message('OK');
					});
			}
			else{
					alertify.alert('Task Creation Failed', function(){
    				alertify.message('OK');
					});
			}	
		}else if(request.readyState == 4 && request.status != 200){
    				
		}
	}
	request.open("POST", "http://danielmcwilliams.com/csci3000/Final/omegaTaskDataAPI.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send(params);
	window.setTimeout(dummySuccess(), 2000);
}
function dummySuccess()
{
		alertify.alert('new Line Created Task Name : ' + title, function(){
    				alertify.message('OK');
					});
}
function populateTasks (options)
{
	for (var i = 0; i < options.length; i++)
	{
		addTask(options[i].guid,options[i].title);
	}
}
function addTask (id,value)
{
	var mySelect = $('#task_select');
	var myOptions = {
     id: value
	};
	$.each(myOptions, function(val, text) {
    mySelect.append(
        $('<option></option>').val(val).html(text)
    );
	});	
}
function handleTime (a,b)
{
    var isnum = /^\d+$/.test(document.getElementById(a).value);
	
	if (isnum)
	{
		$(b).notify('hours is valid',"success");
	}
	else{
		$(b).notify('hours must contain numbers only',"error");
	}
}
function populateTeams (options)
{
	for (var i = 0; i < options.length; i++)
	{
		addTeam(i+1,options[i].title);
	}
}
function addTeam(id,value)
{
	var mySelect = $('#team_select');
	var myOptions = {
     id: value
	};
	$.each(myOptions, function(val, text) {
    mySelect.append(
        $('<option></option>').val(val).html(text)
    );
	});	
}
function populateSelect (options)
{
	for (var i = 0; i < options.length; i++)
	{
		addOption(i+1,options[i].firstName + ", " + options[i].lastName);
	}
}
function addOption (id,value)
{
var anOption = document.createElement("option"); 
// 
var myOptions = {
     id: value
};
var mySelect = $('#user_select');
$.each(myOptions, function(val, text) {
    mySelect.append(
        $('<option></option>').val(val).html(text)
    );
});
/*
var userDropDown = $('#user_select');
anOption.text = value;
anOption.value = value;
userDropDown.add(anOption);
*/
/*
$('#mySelect')
    .find('option')
    .remove()
    .end()
    .append('<option value="whatever">text</option>')
    .val('whatever')
;
*/
}
function fetchSampleTaskList(){
	var params = "f=fetchUsers";
	var request;
	if (window.XMLHttpRequest){ //IE7+, Firefox, Chrome, Opera, Safari
		request = new XMLHttpRequest();
	}else{//IE6, IE5
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	// update the rank value when a response is returned
	request.onreadystatechange=function(){
		if(request.readyState == 4 && request.status == 200){
			// check for error text instead of number
	
			var obj = jQuery.parseJSON( request.responseText );
			var taskList = obj["TaskList"];
			for(var i = 0; i < taskList.length; i++){
				if(taskList[i].isProjectTask){
					var subTask = taskList[i].subTask;
					var subTaskItem = jQuery.parseJSON(taskList[i].subTask[0]);
				}
			}
			var subtaskLinks = obj["SubtaskLinks"];
			taskData = obj;
			createGanttChart();
			createBurndownChart();
		}else if(request.readyState == 4 && request.status != 200){
			// something failed
		}
	}
	request.open("POST", "omegaTaskDataAPI.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send(params);	
}
function handleDescription (a,b)
{
	//alert ("Description getting called");
	//alert (a);
	//alert (b);
	var desc =showLengthWithElement(a);
	/*
	if(desc == null)
	{
		alert ("DESC IS NULL");
	}
	else{
		alert ("DESC IS NOT NULL");
	}
	
	alert ("Description value :" + desc); 
	*/
	if (desc < 10)
	{
		$(b).notify('description must be at least 10 characters long',"error");
	}
	else if (desc > 240)
	{
		$(b).notify('description must be at more than 240 characters long',"error");
	}
	else{
		$(b).notify('description valid',"success");
	}
}
//handleDescription('description','div_Description')
// handleTitle('task_title','.div_Title')
function handleTitle (a,b)
{
	if (showLengthWithElement(a)==0)
	{
		$(b).notify('task must have a title',"error");
	}
	else if (showLengthWithElement(a) > 40)
	{
		$(b).notify('task title can be no longer than 40 characters',"error");
	}
	else{
		$(b).notify('task title is valid',"success");
	}
}
function handleId (a,b)
{
	
    var isnum = /^\d+$/.test(document.getElementById(a).value);
	
	if (isnum)
	{
		$(b).notify('bussiness id is valid',"success");
	}
	else{
		$(b).notify('bussiness id nust contain numbers only',"error");
	}
}
// checkDate('task_mon','task_day','task_year','.div_Startdate')
function checkDate (mon,day,year,b)
{
		//alertify.alert('check date getting called', function(){
    //alertify.message('OK');
	//});
	var month = document.getElementById(mon).value;
	var year = document.getElementById(day).value;
	var length = showLengthWithElement(year);
	var day = document.getElementById(year).value;
	var maxDay = monthForDay(month,year);
	alert ("month : " + month + "\n year: " + year  + "\n Length : " + length + "\n day :" + year + "\n day : " + day+
	"max day : " + maxDay);
	var msg = "";
	if (year == ''|| year == null || month=='' || month==null || day=='' || day==null)
	{
		(b).notify('year,month, and/or day field is not filled out',"error");
		return;
	}
	if (month < 1 || month > 12)
	{
		msg+="month number must be between 1 and 12 \n";
	}
	if (length > 4 || length < 4)
	{
		msg+="year is invalid \n";
	}
	 if (day < 1 || day > maxDay)
	{
	 	msg+="day is invalid ";
	}
	if (msg!="")
	{
		$(b).notify(msg,"error");
	}
	else{
		$(b).notify(msg,"success");
	}
} 
// 31,28,31,30,31,30,31,31,30,31,30,31
function monthForDay (a,b)
{
	switch (a)
	{
		case 1,3,5,7,8,10,12:
		return 31;
		break;
		case 2:
	    return isLeapYear(b);
		break;
		case 4,6,9,11:
		return 30;
		default:
		return 0;
	}
}
function isLeapYear (a)
{
	if (a %100 ==0 && a %400 == 0)
	{
		return 29;
	}
	else if (a % 4 ==0)
	{
		return 29;
	}
	else{
		return 28;
	}
}
function handleConfPass (a,b)
{
	var conf = document.getElementById(a).value;
	var pass = document.getElementById('password').value;
	if (conf === pass)
	{
		$(b).notify('passwords match!',"success");
	}
	else{
		if (showLengthWithElement(b) == 0)
		{
			$(b).notify('password length cannot be zero',"error");
		}
		$(b).notify('passwords must match',"error");
	}
	
}
function validatePassword (a)
{
	// (/^
//(?=.*\d)                //should contain at least one digit
//(?=.*[a-z])             //should contain at least one lower case
//(?=.*[A-Z])             //should contain at least one upper case
//[a-zA-Z0-9]{8,}         //should contain at least 8 from the mentioned characters
//$/)
	var password = document.getElementById(a).value;
	var regex = (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/);
	return regex.test(password);
}
function handleEmail (a,b)
{
	var email = document.getElementById(a).value;
	var isEmailValid = false;
	
	if (validateEmail(email))
	{
			$(b).notify('Email is valid',"success");
	}
	else{
		    $(b).notify('Please enter a valid email(must contain an @ and a .)',"error");
	}
	
}
function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
/*
$(document).ready(function(){
  $("#first_name").click(function(){
	  alert ("on click function getting called");
    $(".div_fName").notify('Please Enter name up to 18 characters!')
  });
});
*/

//$('.elem-demo').notify('Hello Box');
function handleName (a , b ,c)
{
	var length = showLengthWithElement(c);
	if (length > a || length == 0)
	{
	$(b).notify(length + '/' +a, "error");
	}
	else{
		$(b).notify(length + '/'+a,"success");
	}
}
  function showLength(e)
      {
          e = e || window.event;//ie doesn't pass event to callback
          var target = e.target || e.srcElement;//ie== srcElement, good browsers: target
          if (target.tagName.toLowerCase() === 'input' && target.type === 'text')
          {
              document.getElementById(target.id + 'Span').innerHTML = target.value.length;
          }
      }
function showNameBox()
{
	$('.div_fName').notify('Please Enter name up to 18 characters!');
}
function showLengthWithElement (id)
{
	var element = document.getElementById(id);
	//alert ('VALUE FOR LENGTH :' + element.value.length);
	return element.value.length;
}
function validateForm() {
	// ["last_name"]
	var msg = "";
	var isFilledOut =true;
    var fName = document.forms["create_user"]["first_name"].value;
	var lName = document.forms["create_user"]["last_name"].value;
	var email = document.forms["create_user"]["email"].value;
	var password = document.forms["create_user"]["password"].value;
	var confPassword = document.forms["create_user"]["conf_password"].value;
    if (fName == null || fName == "") {
        //alert("First name must be filled out");
		msg+="* First name not filled out!\n"
        isFilledOut = false;
    }
	if (lName == null || lName == "")
	{
		msg+="* Last name not filled out!\n";
		isFilledOut = false;
	}
	 var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		msg+="* Not a valid e-mail address\n";
        isFilledOut = false;
    }
	if (password == null || password == "")
	{
		msg+="* Password must be entered!\n";
		isFilledOut = false;
	}
	else if (confPassword==null || confPassword == "")
	{
		msg+="* please confirm password\n";
		isFilledOut = false;
	}
	if (confPassword != password)
		{
			msg+="* Passwords do not match try again\n";
			isFilledOut =false;
		}
	if (msg!="")
	{
			$.notify.defaults({elementPosition:'right middle'});
	alertify
  .alert(msg, function(){
    alertify.message('OK');
  });
	}
	return isFilledOut;
}
/*
alertify
  .alert("This is an alert dialog.", function(){
    alertify.message('OK');
  });
*/
/*
(function()
  {
      function showLength(e)
      {
          e = e || window.event;//ie doesn't pass event to callback
          var target = e.target || e.srcElement;//ie== srcElement, good browsers: target
          if (target.tagName.toLowerCase() === 'input' && target.type === 'text')
          {
              document.getElementById(target.id + 'Span').innerHTML = target.value.length;
          }
      }
      //bind event listener to the div containing all elements you want 
      // to be 'handled' 
      var mainDiv = document.getElementById('allInputs');
      if (!(mainDiv.addEventListener))
      {
          //IE doesn't have EventListeners, and doesn't support onchange
          //this way, use onfocusout
          mainDiv.attachEvent('onfocusout',showLength);
      }
      else
      {
          mainDiv.addEventListener('change',showLength,false);
      }
  })();
*/
