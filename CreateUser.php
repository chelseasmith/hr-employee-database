<?php
function echoJsAlert ($msg)
{
	echo '<script> alert("'.$msg.'"); </script>';
}
class User
{
	public $userFirstName;
	public $userLastName;
	public $email;
	public $password;
	public $isAffilated;
	public $accountType;
	
	function __construct ($_firstName, $_lastName, $_email, $_password,$_isAffilated,$_accountType)
	{
		$this->accountType = $_accountType;
		$this->email = $_email;
		$this->userFirstName = $_firstName;
		$this->userLastName = $_lastName;
		$this->password = $_password;
	}
	function __destruct ()
	{
		$this->accountType = NULL;
		$this->email = NULL;
		$this->userFirstName = NULL;
		$this->userLastName = NULL;
		$this->password = NULL;
	}
	function toXml ()
	{
	   $xml = '<User>';
	   $xml.='<accountType>'. $this->accountType . '</accountType>';
	   $xml.='<email>'. $this->email . '</email>';
	   $xml.='<fName>' . $this->userFirstName.'</fName>';
	   $xml.='<lName>' . $this->userLastName . '</lName>';
	   $xml.='<affilated>' . $this->isAffilated . '</affilated>';
	   return $xml;
	}
}
class userAPI
{
	private $db;
		function __construct(){//1
		require_once('userConfig.php');
		$this->db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE);
	}
	function __destruct(){
		$this->db->close();
	}
	function getKey ()
	{
		// change to generate a key.
		return 5;
	}
  // insert database information here
  function insertNewUser (User $user)
  {
	  		
		$q = "INSERT INTO Users (fname,lname,email,password,key) VALUES (?,NOW())";
		$stmt = $this->db->prepare($q);
		if (!$this->db->error){
			if ($stmt->bind_param('sssss',$user->userFirstName,$user->userLastName,$user->email,$user->password,$this->getKey()))
			{
				if($stmt->execute()){
					if(!$this->db->error){
						echoJsAlert('User sucessfully created');
					}	
				}else{
					
                }
			}else{
			}
		}else{
		}
  }
}
function userExists (User $user)
{
	$q = "SELEECT * FROM USERS WHERE email LIKE ?";
			$stmt = $this->db->prepare($q);
		if (!$this->db->error){
			if ($stmt->bind_param('s',$user->email))
			{
				if($stmt->execute()){
					if ($stmt->bind_result())
					{
					if(!$this->db->error){
						$count=0;
						while ($stmt->fetch())
						{
						 $count++;	
						}
						if ($count > 0)
						{
							return true;
						}
						else{
							return false;
						}
					}	
				}else{
					
                }
				}
				else{
				}
			}else{
			}
		}else{
		}
  }
?>