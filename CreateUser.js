//$.notify("Hello World");
//$("first_name").on("input", null, null, handleName);
function init ()
{
	$.notify.defaults({elementPosition: 'right middle'});
}

function handlePassword (a,b)
{
	if (validatePassword(a))
	{
		$(b).notify('Password is valid',"success");
	}
    
	else{
		$(b).notify('Password must meet requirements \n'+
		'1. must contain one lower case letter \n'+
		'2. must contain one upper case letter \n'+
		'3. must contain one digit \n'+
		'4. must be at least 8 characters long',"error");
	}
}
function handleId (a,b)
{
    var isnum = /^\d+$/.test(document.getElementById(a).value);
	
	if (isnum)
	{
		$(b).notify('bussiness id is valid',"success");
	}
	else{
		$(b).notify('bussiness id nust contain numbers only',"error");
	}
}
function handleConfPass (a,b)
{
	var conf = document.getElementById(a).value;
	var pass = document.getElementById('password').value;
	if (conf === pass)
	{
		$(b).notify('passwords match!',"success");
	}
	else{
		if (showLengthWithElement(b) == 0)
		{
			$(b).notify('password length cannot be zero',"error");
		}
		$(b).notify('passwords must match',"error");
	}
	
}
function validatePassword (a)
{
	// (/^
//(?=.*\d)                //should contain at least one digit
//(?=.*[a-z])             //should contain at least one lower case
//(?=.*[A-Z])             //should contain at least one upper case
//[a-zA-Z0-9]{8,}         //should contain at least 8 from the mentioned characters
//$/)
	var password = document.getElementById(a).value;
	var regex = (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/);
	return regex.test(password);
}
function handleEmail (a,b)
{
	var email = document.getElementById(a).value;
	var isEmailValid = false;
	
	if (validateEmail(email))
	{
			$(b).notify('Email is valid',"success");
	}
	else{
		    $(b).notify('Please enter a valid email(must contain an @ and a .)',"error");
	}
	
}
function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
/*
$(document).ready(function(){
  $("#first_name").click(function(){
	  alert ("on click function getting called");
    $(".div_fName").notify('Please Enter name up to 18 characters!')
  });
});
*/

//$('.elem-demo').notify('Hello Box');
function handleName (a , b ,c)
{
	var length = showLengthWithElement(c);
	if (length > a || length == 0)
	{
	$(b).notify(length + '/' +a, "error");
	}
	else{
		$(b).notify(length + '/'+a,"success");
	}
}
  function showLength(e)
      {
          e = e || window.event;//ie doesn't pass event to callback
          var target = e.target || e.srcElement;//ie== srcElement, good browsers: target
          if (target.tagName.toLowerCase() === 'input' && target.type === 'text')
          {
              document.getElementById(target.id + 'Span').innerHTML = target.value.length;
          }
      }
function showNameBox()
{
	$('.div_fName').notify('Please Enter name up to 18 characters!');
}
function showLengthWithElement (id)
{
	var element = document.getElementById(id);
	//alert ('VALUE FOR LENGTH :' + element.value.length);
	return element.value.length;
}

function validateForm() {
	// ["last_name"]
	var msg = "";
	var isFilledOut =true;
    var fName = document.forms["create_user"]["first_name"].value;
	var lName = document.forms["create_user"]["last_name"].value;
	var email = document.forms["create_user"]["email"].value;
	var password = document.forms["create_user"]["password"].value;
	var confPassword = document.forms["create_user"]["conf_password"].value;
    if (fName == null || fName == "") {
        //alert("First name must be filled out");
		msg+="* First name not filled out!\n"
        isFilledOut = false;
    }
	if (lName == null || lName == "")
	{
		msg+="* Last name not filled out!\n";
		isFilledOut = false;
	}
	 var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		msg+="* Not a valid e-mail address\n";
        isFilledOut = false;
    }
	if (password == null || password == "")
	{
		msg+="* Password must be entered!\n";
		isFilledOut = false;
	}
	else if (confPassword==null || confPassword == "")
	{
		msg+="* please confirm password\n";
		isFilledOut = false;
	}
	if (confPassword != password)
		{
			msg+="* Passwords do not match try again\n";
			isFilledOut =false;
		}
	if (msg!="")
	{
			$.notify.defaults({elementPosition:'right middle'});
	alertify
  .alert(msg, function(){
    alertify.message('OK');
  });
	}
	return isFilledOut;
}
/*
alertify
  .alert("This is an alert dialog.", function(){
    alertify.message('OK');
  });
*/
/*
(function()
  {
      function showLength(e)
      {
          e = e || window.event;//ie doesn't pass event to callback
          var target = e.target || e.srcElement;//ie== srcElement, good browsers: target
          if (target.tagName.toLowerCase() === 'input' && target.type === 'text')
          {
              document.getElementById(target.id + 'Span').innerHTML = target.value.length;
          }
      }
      //bind event listener to the div containing all elements you want 
      // to be 'handled' 
      var mainDiv = document.getElementById('allInputs');
      if (!(mainDiv.addEventListener))
      {
          //IE doesn't have EventListeners, and doesn't support onchange
          //this way, use onfocusout
          mainDiv.attachEvent('onfocusout',showLength);
      }
      else
      {
          mainDiv.addEventListener('change',showLength,false);
      }
  })();
*/
