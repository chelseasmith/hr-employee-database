<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Project Mgr</title>
<link rel="stylesheet" type="text/css" href="cssReset.css" />
<link rel="stylesheet" type="text/css" href="project.css" />
<script language="javascript" type="text/javascript" src="flot/jquery.js"></script>
<script language="javascript" type="text/javascript" src="flot/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="flot/jquery.flot.pie.js"></script>
<script language="javascript" type="text/javascript" src="flot/jquery.flot.categories.js"></script>
<script language="javascript" type="text/javascript" src="project.js"></script>
</head>
<body onload="init();">
<div id="wrapper">
<div id="graphContainer">
<h1 class="chartLabel">Burn Down Chart</h1>
<h2 class="leftYLabel">Hours Remaining</h2>
<h2 class="rightYLabel">Tasks Remaining</h2>
<div id="placeholder">
</div>
<h2 class="bottomXLabel">Tasks Completed</h2>
</div>
</div>
</body>
</html>
